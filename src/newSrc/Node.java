package newSrc;

public class Node {
    int data; // Data
    Node next; // Address

    public Node(int data) {
        this.data = data;
        this.next = null;
    }

}
